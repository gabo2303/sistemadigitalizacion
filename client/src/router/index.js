import Vue from 'vue'
import Router from 'vue-router'
import Welcome from '@/components/Welcome'
import Documento from '@/components/Documento'
import Login from '@/components/Login'
import Log from '@/components/Log'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/Welcome',
      name: 'Welcome',
      component: Welcome
    },
    {
      path: '/Documento/:idArchivo/:nombreArchivo/:variablesDinamicas',
      name: 'Documento',
      component: Documento
    },
    {
      path: '/Log/:tipoLog',
      name: 'Log',
      component: Log
    }
  ]
})
