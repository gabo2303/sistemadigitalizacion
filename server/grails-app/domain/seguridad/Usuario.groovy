package seguridad

import digitalizacion.Cliente

class Usuario
{
    String username
    String password
    boolean enabled = true
    boolean accountExpired = false
    boolean accountLocked = false
    boolean passwordExpired = false

    static belongsTo = [cliente:Cliente]
}