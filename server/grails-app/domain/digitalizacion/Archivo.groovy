package digitalizacion

class Archivo 
{
	String nombreArchivo
	String variablesDinamicas
	Date fechaCreacion = new Date()
	Boolean activo = true

	static belongsTo = [cliente: Cliente]

    static constraints = 
    { 
    	fechaCreacion nullable:true 
    	activo nullable:true
    }
}