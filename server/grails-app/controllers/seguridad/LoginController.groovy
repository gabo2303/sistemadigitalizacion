package digitalizacion

import seguridad.Usuario

class LoginController  
{
    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    
    def auth() 
    {
        println "ENTRA AUTH"

        def usuario = Usuario.findByUsername(request.JSON.username)

        //def token = usuario.id MEJORA

        def res = []

        if(usuario.password == request.JSON.password)
        {
            res.add('jwt':'ok')    
            res.add('cliente':usuario.cliente.id)
            res.add('usuario':usuario.id)
        }
        else { res.add('jwt':'nok') }

        /*def encoded = request.JSON.password.encodeAsBase64()
        def decoded = new String(encoded.decodeBase64())

        println "ENC: " + encoded

        println "DEC: " + decoded*/

        respond res 

        println "SALE AUTH"
    }
}