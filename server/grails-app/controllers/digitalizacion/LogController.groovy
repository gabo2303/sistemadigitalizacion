package digitalizacion

import seguridad.Usuario
import java.text.SimpleDateFormat

class LogController  
{
    static responseFormats = ['json']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    
    def listadoLogs() 
    {
        println "ENTRA listadoLogs"

        def tipoLog

        println request.JSON.tipoLog

        if (request.JSON.tipoLog == '1' || request.JSON.tipoLog == 1)
        {
            tipoLog = 'CambioEstado'
        }
        else { tipoLog = 'Error' }

        println tipoLog

        params.max = 10
        params.offset = request.JSON.offset

        def logs = Log.createCriteria().list(params)
        {
            eq('cliente', Cliente.get(request.JSON.idCliente))
            eq('tipoLog', tipoLog)
            
            order 'fecha', 'desc'
        }

        println logs.totalCount

        def res = [:]

        int paginas = logs.totalCount / 10 

        if( (logs.totalCount % 10) == 1) { paginas++ }

        res.logsTotales = logs
        res.pages = paginas

        respond res

        println "SALE listadoLogs"
    }
}