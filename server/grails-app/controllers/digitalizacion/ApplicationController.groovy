package digitalizacion

import grails.core.GrailsApplication
import grails.plugins.*

import org.apache.pdfbox.pdmodel.*
import org.fit.pdfdom.*

import org.apache.poi.*
import org.apache.commons.io.FileUtils
import javax.swing.JLabel
import java.awt.image.BufferedImage
import javax.imageio.ImageIO
import java.awt.Graphics
import java.awt.Color
import gui.ava.html.image.generator.HtmlImageGenerator

import org.xhtmlrenderer.pdf.ITextRenderer

import java.nio.file.Files
import java.nio.file.Paths

import java.util.Base64

import seguridad.Usuario

class ApplicationController implements PluginManagerAware 
{
	def springSecurityService
	def wkhtmltoxService

    def pathGlobal = "C:/Workspace/webapps/"

    GrailsApplication grailsApplication
    GrailsPluginManager pluginManager
	
	static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    
    def index() { [grailsApplication: grailsApplication, pluginManager: pluginManager] }

    def procesaPdf() 
    {
    	println 'ENTRA procesaPdf'

    	def archivo = request.JSON.archivo + ".pdf"
    	//File file = new File(grailsApplication.mainContext.servletContext.getRealPath('') + archivo)
        File file = new File(pathGlobal + archivo)

        FileOutputStream fos = new FileOutputStream(file)
        
        byte[] decode = request.JSON.pdf.decodeBase64()

		fos.write(decode)
        
        fos.flush()
        fos.close()
        
    	def res = []

    	//File pdf = grailsApplication.mainContext.getResource(archivo).file

    	try 
        {
        	//PDDocument pdfCargado = PDDocument.load(pdf) 
            PDDocument pdfCargado = PDDocument.load(file) 
        	
            //Writer output = new PrintWriter("src/main/webapp/" + request.JSON.archivo +".html", "UTF-8");
    		Writer output = new PrintWriter(pathGlobal + request.JSON.archivo +".html", "UTF-8");

            new PDFDomTree().writeText(pdfCargado, output);
      		
			//File html = grailsApplication.mainContext.getResource(request.JSON.archivo + ".html").file
            File html = new File(pathGlobal + request.JSON.archivo + ".html")

            pdfCargado.close()
    		output.close()

            res.add('pdf':html.getText('UTF-8'))

            //pdf.delete()
            html.delete()
        }
        catch (Exception e) { e.printStackTrace() }
    	
    	respond res 

        println 'SALE procesaPdf'
    }

    def guardaHtml() 
    {
    	println 'ENTRA guardaHtml'

        println request.JSON.variables
        println request.JSON.idArchivo

        def res = []

        try 
        {
            if(request.JSON.idArchivo == 0 || request.JSON.idArchivo == '0')
            {
                println('entra 1')
                new Archivo(nombreArchivo:request.JSON.nombreArchivo, 
                        variablesDinamicas:request.JSON.variables,
                        cliente: Cliente.get(request.JSON.cliente))
                        .save (flush:true, failOnError:true)
            }
            else
            {
                def archivo = Archivo.get(request.JSON.idArchivo)

                archivo.variablesDinamicas = request.JSON.variables

                archivo.save (flush:true)
            }

            //File htmlEditable = new File(grailsApplication.mainContext.servletContext.getRealPath('') + 
            File htmlEditable = new File(pathGlobal + request.JSON.nombreArchivo + "Editable.html");

            FileUtils.writeStringToFile(htmlEditable, request.JSON.html, 'UTF-8');

            def arrayPrueba = request.JSON.html.split('<div')
            def stringPre

            arrayPrueba.eachWithIndex
            {
                prueba, d ->

                if(d == 0) { stringPre = arrayPrueba[0] }
            
                else
                {
                    stringPre = stringPre + "<div"

                    if(prueba.contains('class="p"')) 
                    { 
                        def val = prueba.substring(prueba.indexOf('width') + 6, prueba.indexOf('width') + 8);
                        def nuevoVal = val.replace(".", "").toInteger() + 10
                        def nuevaPrueba = prueba.replace("width:" + val, "width:" + nuevoVal.toString())
                        
                        stringPre = stringPre + nuevaPrueba
                    }
                    else { stringPre = stringPre + prueba }
                }
            }

            def width = (new Long(request.JSON.width) / 2.834645669291339).round(1)
            def height = (new Long(request.JSON.height) / 2.834645669291339).round(1)

            def stringFormateado = "<html>\n<head>".concat(stringPre.drop(125)).concat("</body>\n</html>")
            def str2 = stringFormateado.replace(
                        ".page{position:relative; border:1px solid blue;margin:0.5em}", 
                        ".page{position:relative; margin:0.5em}") 
            
            def str3 = str2.replace(
                        "@supports(-webkit-text-stroke: 1px black) {.p{text-shadow:none !important;}}", 
                        "@page{ margin-right:0px; margin-left:-17px; margin-top:-7.5px; margin-bottom:0px; size:" + width + "mm " + height +
                        "mm;}\n@supports(-webkit-text-stroke: 1px black) {.p{text-shadow:none !important;}}")

            def str = str3.replace("</style>", "</style> \n </head> \n <body>") 

            def arreglo = str.split('<img') 
            def arregloImg

            arreglo.eachWithIndex
            {
                ar, i ->

                if(i == 0) { arregloImg = arreglo[0] }
                
                else
                {
                    def array = ar.replaceFirst(">", "/>")

                    arregloImg = arregloImg + "<img" + array    
                }
            }   

            //File newHtmlFile = new File(grailsApplication.mainContext.servletContext.getRealPath('') + request.JSON.nombreArchivo + ".html");
            File newHtmlFile = new File(pathGlobal + request.JSON.nombreArchivo + ".html");
            FileUtils.writeStringToFile(newHtmlFile, arregloImg, 'UTF-8');    
        }
        catch(Exception e) 
        {
            e.printStackTrace()  

            def usuario = Usuario.get(request.JSON.idUsuario)

            new Log(tipoLog:'Error', descripcion:e.getMessage(), username:usuario.username, 
                cliente:usuario.cliente).save(flush:true, failOnError:true)          
        }
        finally  { respond res }

    	println 'FINALIZA guardaHtml'
    }

    def editaHtml() 
    {
        println 'ENTRA editaHtml'

        def res = [] 

        try 
        {            
            //File html = grailsApplication.mainContext.getResource(request.JSON.archivo + "Editable.html").file

            File html = new File(pathGlobal + request.JSON.archivo + "Editable.html")

            res.add('pdf':html.getText('UTF-8'))
        }
        catch (Exception e) { e.printStackTrace() }
        
        respond res 

        println 'FINALIZA editaHtml'
    }    

    def generaDigitalizacion()
    {
    	println "ENTRA generaDigitalizacion"

    	println request.JSON.nombrePdf

        def res = []

		try
        {
            String content = new String(Files.readAllBytes(
						 Paths.get(grailsApplication.mainContext.servletContext.getRealPath('') + "prueba5.html")), "UTF-8")
    		String todoFinal

        	request.JSON.each 
            {
        		if(it.key != "nombrePdf" && !it.key.contains('X'))
        		{
        			content = content.replace('$' + it.key, it.value)
        		}
                else if(it.key != "nombrePdf" && it.key.contains('X'))
                {
                    content = content.replace('$' + it.key, it.value)
                }
        	}

    		File newHtmlFile = new File(grailsApplication.mainContext.servletContext.getRealPath('') + "prueba5.html");
    		FileUtils.writeStringToFile(newHtmlFile, content, 'UTF-8');


        	File dest = new File(grailsApplication.mainContext.servletContext.getRealPath('') + "html.pdf")

    		String url = new File(grailsApplication.mainContext.servletContext.getRealPath('') + "prueba5.html").toURL().toString()

    		println "URL: " + url

    		OutputStream out = new FileOutputStream(dest)

    		ITextRenderer renderer = new ITextRenderer()

    		renderer.setDocument(url)
    		renderer.layout()
    		
    		renderer.createPDF(out)

    		out.close()

    		def codificarPdf = new String(Files.readAllBytes(
    						 Paths.get(grailsApplication.mainContext.servletContext.getRealPath('') + "html.pdf")), "UTF-8")

    		def encoded = codificarPdf.getBytes("UTF-8").encodeBase64().toString()
    		
    		res.add('codificado':encoded)
    		res.add('status':'ok') 
        }
        catch(Exception e)
        {
            res.add('error':e.getMessage())

            new Log(tipoLog:'Error', descripcion:e.getMessage(), username:'prueba', 
                cliente:Cliente.get(13)).save(flush:true, failOnError:true)
        }
        finally
        {
            respond res
        }

	    println "FINALIZA generaDigitalizacion"  
    }
}