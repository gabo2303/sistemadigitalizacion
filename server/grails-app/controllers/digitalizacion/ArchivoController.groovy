package digitalizacion

import seguridad.Usuario

class ArchivoController  
{
    static responseFormats = ['json']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    
    def listadoArchivos() 
    {
        println "ENTRA listadoArchivos"

        params.max = 10

        def archivos = Archivo.createCriteria().list(params)
        {
            eq('cliente', Cliente.get(13))            

            order 'activo', 'desc'
        }

        println archivos.totalCount
        
        def res = [:]

        res.archivosTotales = archivos

        respond res

        println "SALE listadoArchivos"
    }

    def cambiarEstadoArchivo()
    {
        println "ENTRA cambiarEstadoArchivo"

        def res = [:]

        try
        {
            def archivo = Archivo.get(request.JSON.idArchivo)    

            /*File editable = new File(grailsApplication.mainContext.servletContext.getRealPath('') 
                            + archivo.nombreArchivo + "Editable.html");
            
            File normal = new File(grailsApplication.mainContext.servletContext.getRealPath('') 
                          + archivo.nombreArchivo + ".html");

            editable.delete()
            normal.delete()

            archivo.delete(flush:true)*/

            archivo.activo = !archivo.activo 
            archivo.save(flush:true, failOnError:true)

            def usuario = Usuario.get(request.JSON.idUsuario)

            new Log(username:usuario.username, tipoLog:'CambioEstado', 
                descripcion:'Se ' + (!archivo.activo ? 'activo' : 'desactivo') 
                + ' el archivo ' + archivo.nombreArchivo + '.pdf por el usuario ' 
                + usuario.username, cliente:usuario.cliente).save(flush:true, failOnError:true)
            
            res.archivosTotales = Archivo.findAllByCliente(Cliente.get(13), [sort:'activo'])

            res.status = 'ok' 
        }
        catch(Exception e) 
        {
            e.printStackTrace()

            res.status = 'nok' 
        }

        respond res        

        println "SALE cambiarEstadoArchivo"
    }
}