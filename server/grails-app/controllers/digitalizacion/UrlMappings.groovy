package digitalizacion

import grails.util.Environment

class UrlMappings {

    static mappings = {
        delete "/$controller/$id(.$format)?"(action:"delete")
        get "/$controller(.$format)?"(action:"index")
        get "/$controller/$id(.$format)?"(action:"show")
        post "/$controller(.$format)?"(action:"save")
        put "/$controller/$id(.$format)?"(action:"update")
        patch "/$controller/$id(.$format)?"(action:"patch")

        "/Application/procesaPdf?"(controller: 'application', action:'procesaPdf', method:"POST")
        "/Application/guardaHtml?"(controller: 'application', action:'guardaHtml', method:"POST")
        "/Application/editaHtml?"(controller: 'application', action:'editaHtml', method:"POST")
        "/Application/generaDigitalizacion?"(controller: 'application', action:'generaDigitalizacion', method:"POST")
        "/login/auth?"(controller: 'login', action:'auth', method:"POST")
        "/archivo/listadoArchivos?"(controller: 'archivo', action:'listadoArchivos', method:"GET")
        "/archivo/cambiarEstadoArchivo?"(controller: 'archivo', action:'cambiarEstadoArchivo', method:"POST")
        "/log/listadoLogs?"(controller: 'log', action:'listadoLogs', method:"POST")
        
        if ( Environment.current == Environment.PRODUCTION ) { '/'(uri: '/index.html') } 
        
        else { '/'(controller: 'application', action:'index') }

        "500"(view: '/error')
        "404"(view: '/notFound')
    }
} 
